package Modelo;

import com.example.examencorteiijava.Productos;

public interface Persistencia {
    public void openDataBase();
    public void closeDataBase();
    public long insertarProducto(Productos producto);
}
