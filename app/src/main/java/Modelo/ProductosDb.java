package Modelo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.examencorteiijava.Productos;

public class ProductosDb implements Persistencia, Proyeccion{
    private Context context;
    private ProductoDbHelper helper;
    private SQLiteDatabase db;

    public ProductosDb(Context context, ProductoDbHelper helper){
        this.context = context;
        this.helper = helper;
    }

    public ProductosDb(Context context){
        this.context = context;
        this.helper = new ProductoDbHelper(this.context);
    }

    @Override
    public void openDataBase() {
        db = helper.getWritableDatabase();
    }

    @Override
    public void closeDataBase() {
        helper.close();
    }

    @Override
    public long insertarProducto(Productos producto) {
        ContentValues values = new ContentValues();

        values.put(DefineTabla.Productos.COLUMN_NAME_CODIGO, producto.getCodigo());
        values.put(DefineTabla.Productos.COLUMN_NAME_NOMBRE, producto.getNombre());
        values.put(DefineTabla.Productos.COLUMN_NAME_MARCA, producto.getMarca());
        values.put(DefineTabla.Productos.COLUMN_NAME_PRECIO, Productos.getPrecio());
        values.put(DefineTabla.Productos.COLUMN_NAME_TIPO, producto.getTipo());


        this.openDataBase();
        long num = db.insert(DefineTabla.Productos.TABLE_NAME, null, values);
        Log.d("Agregar", "Se insertó el producto" + num);

        return num;
    }

    @Override
    public boolean getProducto(String codigo, String nombre, String marca, float precio, String tipo) {
        db = helper.getReadableDatabase();

        Cursor cursor = db.query(
                DefineTabla.Productos.TABLE_NAME,
                DefineTabla.REGISTROS,
                DefineTabla.Productos.COLUMN_NAME_CODIGO + " = ? ",
                new String[] {codigo},
                null,
                null,
                null
        );

        boolean productoEncontrado = false;

        if (cursor.moveToFirst()) {
            productoEncontrado = true;

            String nombreProducto = cursor.getString(cursor.getInt(2));
            String marcaProducto = cursor.getString(cursor.getInt(3));
            float precioProducto = cursor.getFloat(cursor.getInt(4));
            String tipoProducto = cursor.getString(cursor.getInt(5));
        }

        cursor.close();
        db.close();

        return productoEncontrado;
    }

    @Override
    public boolean getProducto(String codigo) {
        db = helper.getReadableDatabase();

        Cursor cursor = db.query(
                DefineTabla.Productos.TABLE_NAME,
                DefineTabla.REGISTROS,
                DefineTabla.Productos.COLUMN_NAME_CODIGO + " = ? ",
                new String[] {codigo},
                null,
                null,
                null
        );

        boolean productoEncontrado = false;

        if (cursor.moveToFirst()) {
            productoEncontrado = true;

            String nombreProducto = cursor.getString(cursor.getInt(2));
            String marcaProducto = cursor.getString(cursor.getInt(3));
            float precioProducto = cursor.getFloat(cursor.getInt(4));
            String tipoProducto = cursor.getString(cursor.getInt(5));

            mandarDatos(nombreProducto, marcaProducto, precioProducto, tipoProducto);
        }

        cursor.close();
        db.close();

        return productoEncontrado;
    }


    @Override
    public Productos readProducto(Cursor cursor) {
        return null;
    }

    public String[] mandarDatos(String nombre, String marca, float precio, String tipo) {

        String[] datos = {nombre, marca, String.valueOf(precio) ,tipo};

        return datos;
    }

    public Cursor buscarPorNombre(String codigo) {
        db = helper.getReadableDatabase();

        String[] projection = {
                DefineTabla.Productos.COLUMN_NAME_CODIGO,
                DefineTabla.Productos.COLUMN_NAME_NOMBRE,
                DefineTabla.Productos.COLUMN_NAME_MARCA,
                DefineTabla.Productos.COLUMN_NAME_PRECIO,
                DefineTabla.Productos.COLUMN_NAME_TIPO
        };

        String selection = DefineTabla.Productos.COLUMN_NAME_CODIGO + " LIKE ?";
        String[] selectionArgs = {"%" + codigo + "%"};

        Cursor cursor = db.query(
                DefineTabla.Productos.TABLE_NAME,
                projection,
                selection,
                selectionArgs,
                null,
                null,
                null
        );

        return cursor;
    }
}
