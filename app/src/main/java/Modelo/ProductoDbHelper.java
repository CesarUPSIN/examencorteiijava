package Modelo;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class ProductoDbHelper extends SQLiteOpenHelper {
    private static  final String TEXT_TYPE = " TEXT";
    private static final String COMA_SEP = " ,";

    private static final String SQL_CREATE_PRODUCTO = "CREATE TABLE " +
            DefineTabla.Productos.TABLE_NAME + " (" +
            DefineTabla.Productos.COLUMN_NAME_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            DefineTabla.Productos.COLUMN_NAME_CODIGO + TEXT_TYPE + COMA_SEP +
            DefineTabla.Productos.COLUMN_NAME_NOMBRE + TEXT_TYPE + COMA_SEP +
            DefineTabla.Productos.COLUMN_NAME_MARCA + TEXT_TYPE + COMA_SEP +
            DefineTabla.Productos.COLUMN_NAME_PRECIO + " REAL" + COMA_SEP +
            DefineTabla.Productos.COLUMN_NAME_TIPO + TEXT_TYPE + ")";

    private static final String SQL_DELETE_PRODUCTO = "DROP TABLE IF EXISTS " +
            DefineTabla.Productos.TABLE_NAME;

    private static final String DATABASE_NAME = "sistema002.db";

    private static final int DATABASE_VERSION = 1;

    public ProductoDbHelper (Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_PRODUCTO);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_PRODUCTO);
        onCreate(db);
    }

}
