package Modelo;

import android.database.Cursor;

import com.example.examencorteiijava.Productos;

public interface Proyeccion {
    public boolean getProducto(String codigo, String nombre, String marca, float precio, String tipo);

    boolean getProducto(String codigo);

    public Productos readProducto(Cursor cursor);
}
