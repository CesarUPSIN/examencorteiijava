package com.example.examencorteiijava;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import Modelo.ProductosDb;

public class MainActivity extends AppCompatActivity {
    private EditText edtCodigo, edtNombre, edtMarca, edtPrecio;
    private RadioButton rbtPerecedero, rbtNoPerecedero;
    private Button btnGuardar, btnLimpiar, btnNuevo, btnEditar;
    private Productos producto;
    private ProductosDb productoDb;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edtCodigo = findViewById(R.id.edtCodigo);
        edtNombre = findViewById(R.id.edtNombre);
        edtMarca = findViewById(R.id.edtMarca);
        edtPrecio = findViewById(R.id.edtPrecio);
        btnGuardar = findViewById(R.id.btnGuardar);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnNuevo = findViewById(R.id.btnNuevo);
        btnEditar = findViewById(R.id.btnEditar);
        rbtPerecedero = findViewById(R.id.rbtPerecedero);
        rbtNoPerecedero = findViewById(R.id.rbtNoPerecedero);

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registrar();
            }
        });

        btnEditar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editar();
            }
        });
    }

    private void registrar() {
        producto = new Productos();
        producto.setCodigo(edtCodigo.getText().toString());
        producto.setNombre(edtNombre.getText().toString());
        producto.setMarca(edtMarca.getText().toString());
        producto.setPrecio(Float.valueOf(edtPrecio.getText().toString()));

        if(rbtPerecedero.isChecked()) {
            producto.setTipo("Perecedero");
        }
        else if(rbtNoPerecedero.isChecked()) {
            producto.setTipo("No perecedero");
        }

        productoDb = new ProductosDb(getApplicationContext());
        productoDb.insertarProducto(producto);
        Toast.makeText(getApplicationContext(), "Se ha insertado con exito ",Toast.LENGTH_SHORT).show();
        setResult(Activity.RESULT_OK);

    }

    private void editar() {
        Intent intent = new Intent(MainActivity.this, ProductosActivity.class);
        startActivity(intent);
    }

    private boolean validar(){
        if (edtCodigo.getText().toString().equals("")) return false;
        if (edtNombre.getText().toString().equals("")) return false;
        if (edtMarca.getText().toString().equals("")) return false;
        if(edtPrecio.getText().toString().equals("")) return true;

        return false;
    }
}