package com.example.examencorteiijava;

public class Productos {
    private int ID;
    private String codigo;
    private String nombre;
    private String marca;
    private static float precio;
    private String tipo;

    public Productos() {
        this.ID = 0;
        this.codigo = "";
        this.nombre = "";
        this.marca = "";
        this.precio = 0.0f;
        this.tipo = "";
    }

    public Productos(int id, String codigo, String nombre, String marca, float precio, String tipo) {
        this.ID = id;
        this.codigo = codigo;
        this.nombre = nombre;
        this.marca = marca;
        this.precio = precio;
        this.tipo = tipo;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public static float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
}
