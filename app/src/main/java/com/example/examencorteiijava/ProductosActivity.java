package com.example.examencorteiijava;

import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;

import androidx.appcompat.app.AppCompatActivity;

import Modelo.DefineTabla;
import Modelo.ProductosDb;

public class ProductosActivity extends AppCompatActivity {
    private EditText edtCodigo, edtNombre, edtMarca, edtPrecio;
    private RadioButton rbtPerecedero, rbtNoPerecedero;
    private Button btnBuscar, btnBorrar, btnActualizar, btnCerrar;

    private ProductosDb pdb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_productos);

        edtCodigo = findViewById(R.id.edtCodigo);
        edtNombre = findViewById(R.id.edtNombre);
        edtMarca = findViewById(R.id.edtMarca);
        edtPrecio = findViewById(R.id.edtPrecio);
        btnBuscar = findViewById(R.id.btnBuscar);
        btnBorrar = findViewById(R.id.btnBorrar);
        btnActualizar = findViewById(R.id.btnActualizar);
        btnCerrar = findViewById(R.id.btnCerrar);
        rbtPerecedero = findViewById(R.id.rbtPerecedero);
        rbtNoPerecedero = findViewById(R.id.rbtNoPerecedero);
        pdb = new ProductosDb(this);

        btnBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buscar();
            }
        });
    }

    private void buscar() {
        String codigo = edtCodigo.getText().toString().trim();

        Cursor cursor = pdb.buscarPorNombre(codigo);

        if (cursor.moveToFirst()) {

            String nombreProducto = cursor.getString(cursor.getColumnIndexOrThrow(DefineTabla.Productos.COLUMN_NAME_NOMBRE));
            String marcaProducto = cursor.getString(cursor.getColumnIndexOrThrow(DefineTabla.Productos.COLUMN_NAME_MARCA));
            float precioProducto = cursor.getFloat(cursor.getColumnIndexOrThrow(DefineTabla.Productos.COLUMN_NAME_PRECIO));
            String tipoProducto = cursor.getString(cursor.getColumnIndexOrThrow(DefineTabla.Productos.COLUMN_NAME_TIPO));

            edtNombre.setText(nombreProducto);
            edtMarca.setText(marcaProducto);
            edtPrecio.setText(String.valueOf(precioProducto));
            if ("Perecedero".equals(tipoProducto)) {
                rbtPerecedero.setChecked(true);
            } else if ("No Perecedero".equals(tipoProducto)) {
                rbtNoPerecedero.setChecked(true);
            }
        } else {
            edtCodigo.setText("");
            edtMarca.setText("");
            edtPrecio.setText("");
            rbtPerecedero.setChecked(false);
            rbtNoPerecedero.setChecked(false);
        }
    }

}
